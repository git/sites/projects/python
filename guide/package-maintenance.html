<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Python package maintenance &#8212; Gentoo Python Guide  documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=d1102ebc" />
    <link rel="stylesheet" type="text/css" href="_static/basic.css?v=686e5160" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css?v=27fed22d" />
    <script src="_static/documentation_options.js?v=5929fcd5"></script>
    <script src="_static/doctools.js?v=9bcbadda"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Maintenance of Python implementations" href="interpreter-maintenance.html" />
    <link rel="prev" title="QA checks and warnings" href="qawarn.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  

  
  

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="python-package-maintenance">
<h1>Python package maintenance<a class="headerlink" href="#python-package-maintenance" title="Link to this heading">¶</a></h1>
<section id="package-name-policy">
<h2>Package name policy<a class="headerlink" href="#package-name-policy" title="Link to this heading">¶</a></h2>
<p>All packages in <code class="docutils literal notranslate"><span class="pre">dev-python/*</span></code> that are published on <a class="reference external" href="https://pypi.org/">PyPI</a>, must be
named to match their respective PyPI names, after performing
<a class="reference external" href="https://peps.python.org/pep-0503/#normalized-names">normalization specified in PEP 503</a>, i.e. after replacing all runs
of dot, hyphen and underscore characters (<code class="docutils literal notranslate"><span class="pre">[-_.]+</span></code>) with a single
hyphen (<code class="docutils literal notranslate"><span class="pre">-</span></code>), and all uppercase letters with lowercase.  Notably,
prefixes and suffixes such as <code class="docutils literal notranslate"><span class="pre">python-</span></code> must not be removed or added.</p>
<p>Since it is not uncommon for multiple packages to be published with very
similar names, it is crucial that all packages conform to this policy.
Gentoo-specific renames not only make it harder for users to find
the specific package they need but can also create name conflicts when
another package needs to be added.  For example, adding
<code class="docutils literal notranslate"><span class="pre">python-bugzilla</span></code> package as <code class="docutils literal notranslate"><span class="pre">dev-python/bugzilla</span></code> would conflict
with the <code class="docutils literal notranslate"><span class="pre">bugzilla</span></code> package that is also present on PyPI.</p>
<p>The following table illustrates mapping PyPI package names to Gentoo
package names.</p>
<blockquote>
<div><table class="docutils align-default">
<thead>
<tr class="row-odd"><th class="head"><p>PyPI package name</p></th>
<th class="head"><p>Correct Gentoo package name</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>ConfigArgParse</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/configargparse</span></code></p></td>
</tr>
<tr class="row-odd"><td><p>Flask</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/flask</span></code></p></td>
</tr>
<tr class="row-even"><td><p>flask-babel</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/flask-babel</span></code></p></td>
</tr>
<tr class="row-odd"><td><p>github3.py</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/github3-py</span></code></p></td>
</tr>
<tr class="row-even"><td><p>python-bugzilla</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/python-bugzilla</span></code></p></td>
</tr>
<tr class="row-odd"><td><p>sphinx_pytest</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/sphinx-pytest</span></code></p></td>
</tr>
<tr class="row-even"><td><p>sphinx-tabs</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/sphinx-tabs</span></code></p></td>
</tr>
<tr class="row-odd"><td><p>txAMQP</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/txamqp</span></code></p></td>
</tr>
<tr class="row-even"><td><p>zope.interface</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/zope-interface</span></code></p></td>
</tr>
<tr class="row-odd"><td><p>zstd</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">dev-python/zstd</span></code></p></td>
</tr>
</tbody>
</table>
</div></blockquote>
<p>Note that the presented table already presents some inconsistency
in upstream package names.  For example, <code class="docutils literal notranslate"><span class="pre">Flask</span></code> itself uses title
case name, while <code class="docutils literal notranslate"><span class="pre">flask-babel</span></code> has recently switched to lowercase.
Using normalized names for all packages avoids this inconsistency within
Gentoo.  It may also be a good idea to point upstream to <a class="reference external" href="https://peps.python.org/pep-0423/">PEP 423</a>
that specifies package naming recommendations.</p>
<p>PyPI automatically redirects to canonical package URLs.  However, please
make sure to use these canonical URLs in <code class="docutils literal notranslate"><span class="pre">HOMEPAGE</span></code>, <code class="docutils literal notranslate"><span class="pre">SRC_URI</span></code>
and <code class="docutils literal notranslate"><span class="pre">remote-id</span></code> to avoid unnecessary redirects.  These are reported
by <code class="docutils literal notranslate"><span class="pre">pkgcheck</span> <span class="pre">scan</span> <span class="pre">--net</span></code>.</p>
<p>When packaging software that is not published on PyPI, or adding
multiple Gentoo packages corresponding to the same PyPI project, please
bear potential future collisions in mind when naming it.  Avoid names
that are already used for other PyPI projects.  When in doubt, prefer
more verbose names that are less likely to be reused in the future.  You
may also suggest that upstream publishes to PyPI, or at least pushes
an empty package to reserve the name.</p>
</section>
<section id="support-for-python-2">
<h2>Support for Python 2<a class="headerlink" href="#support-for-python-2" title="Link to this heading">¶</a></h2>
<p>Gentoo does not support building Python packages for Python 2 anymore.
We are keeping PyPy2.7 (built stripped down, by default) to facilitate
building PyPy3, and CPython 2.7 for PyPy2.7 bootstrap.  Technically,
they could still be used to run Python 2 code standalone, but this
is discouraged and poses security risk.</p>
</section>
<section id="which-implementations-to-test-new-packages-for">
<h2>Which implementations to test new packages for?<a class="headerlink" href="#which-implementations-to-test-new-packages-for" title="Link to this heading">¶</a></h2>
<p>The absolute minimum set of targets are the current default targets
found in <code class="docutils literal notranslate"><span class="pre">profiles/base/make.defaults</span></code>.  However, developers
are strongly encouraged to test at least the next Python 3 version
in order to ease future transition, and preferably all future versions.</p>
<p>Marking for PyPy3 is optional.  At this moment, we do not aim for wide
coverage of PyPy3 support.</p>
</section>
<section id="adding-new-python-implementations-to-existing-packages">
<h2>Adding new Python implementations to existing packages<a class="headerlink" href="#adding-new-python-implementations-to-existing-packages" title="Link to this heading">¶</a></h2>
<p>New Python implementations can generally be added to existing packages
without a revision bump.  This is because the new dependencies are added
conditionally to new USE flags.  Since the existing users can not have
the new flags enabled, the dependencies do not need to be proactively
added to existing installations.</p>
<p>This usually applies to stable packages as well as new Python targets
are generally <code class="docutils literal notranslate"><span class="pre">use.stable.mask</span></code>-ed.  This means that stable users
will not be able to enable newly added flags and therefore the risk
of the change breaking stable systems is minimal.</p>
</section>
<section id="which-packages-can-be-co-maintained-by-the-python-project">
<h2>Which packages can be (co-)maintained by the Python project?<a class="headerlink" href="#which-packages-can-be-co-maintained-by-the-python-project" title="Link to this heading">¶</a></h2>
<p>A large part of the Python ecosystem is fairly consistent, making it
feasible for (co-)maintenance by the Gentoo Python team.</p>
<p>As a rule of thumb, Python team is ready to maintain packages specific
to the Python ecosystem and useful for the general population of Python
programmers.  This includes Python interpreters and tooling, packages
purely providing Python modules and extensions and utilities specific
to the Python language.</p>
<p>However, the Python team has limited manpower, therefore it may reject
packages that have high maintenance requirements.  As a rule, Python
team does not accept packages without working tests.</p>
<p>If your package matches the above profile, feel free to ask a member
of the Python project whether they would like to (co-)maintain
the package.  However, if you are not a member of the project, please
do not add us without asking first.</p>
</section>
<section id="porting-packages-to-a-new-eapi">
<h2>Porting packages to a new EAPI<a class="headerlink" href="#porting-packages-to-a-new-eapi" title="Link to this heading">¶</a></h2>
<p>When porting packages to a new EAPI, please take care not to port
the dependencies of Portage prematurely.  This generally includes
<code class="docutils literal notranslate"><span class="pre">app-portage/gemato</span></code>, <code class="docutils literal notranslate"><span class="pre">dev-python/setuptools</span></code> and their recursive
dependencies.</p>
<p>Ideally, these ebuilds carry an appropriate note above their EAPI line,
e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="c1"># please keep this ebuild at EAPI 7 -- sys-apps/portage dep</span>
<span class="nv">EAPI</span><span class="o">=</span><span class="m">7</span>
</pre></div>
</div>
<p>This does not apply to test dependencies — they are not strictly
necessary to install a new Portage version.</p>
</section>
<section id="monitoring-new-package-versions">
<h2>Monitoring new package versions<a class="headerlink" href="#monitoring-new-package-versions" title="Link to this heading">¶</a></h2>
<section id="pypi-release-feeds">
<h3>PyPI release feeds<a class="headerlink" href="#pypi-release-feeds" title="Link to this heading">¶</a></h3>
<p>The most efficient way to follow new Python package releases are
the feeds found on <a class="reference external" href="https://pypi.org/">PyPI</a>.  These can be found in the package’s
“Release history” tab, as “RSS feed”.</p>
<p>The Gentoo Python project maintains a comprehensive <a class="reference external" href="https://projects.gentoo.org/python/release-feeds.opml">list of PyPI feeds
for packages</a> in <code class="docutils literal notranslate"><span class="pre">dev-python/</span></code> category (as well as other important
packages maintained by the Python team) in OPML format.</p>
</section>
<section id="checking-via-pip">
<h3>Checking via pip<a class="headerlink" href="#checking-via-pip" title="Link to this heading">¶</a></h3>
<p>The <a class="reference internal" href="#pip-list-outdated">pip list --outdated</a> command described in a followup section
can also be used to verify installed packages against their latest PyPI
releases.  However, this is naturally limited to packages installed
on the particular system, and does not account for newer versions being
already available in the Gentoo repository.</p>
</section>
<section id="repology">
<h3>Repology<a class="headerlink" href="#repology" title="Link to this heading">¶</a></h3>
<p><a class="reference external" href="https://repology.org/">Repology</a> provides a comprehensive service for tracking distribution
package versions and upstream releases.  The easiest ways to find Python
packages present in the Gentoo repository is to search by their
maintainer’s e-mail or category (e.g. <code class="docutils literal notranslate"><span class="pre">dev-python</span></code>).  When searching
by name, the majority of Python-specific package use <code class="docutils literal notranslate"><span class="pre">python:</span></code> prefix
in their Repology names.</p>
<p>Unfortunately, Repology is very susceptible to false positives.
Examples of false positives include other distributions using custom
version numbers, replacing packages with forks or simply Repology
confusing different packages with the same name.  If you find false
positives, please use the ‘Report’ option to request a correction.</p>
<p>Please also note that Repology is unable to handle the less common
version numbers that do not have a clear mapping to Gentoo version
syntax (e.g. <code class="docutils literal notranslate"><span class="pre">.post</span></code> releases).</p>
</section>
</section>
<section id="stabilization-recommendations">
<h2>Stabilization recommendations<a class="headerlink" href="#stabilization-recommendations" title="Link to this heading">¶</a></h2>
<section id="policy">
<h3>Policy<a class="headerlink" href="#policy" title="Link to this heading">¶</a></h3>
<p>The Python landscape is changing dynamically, and therefore the test
suites in packages — if not whole packages — often start failing early.
For this reason, we recommend lowering the baseline stabilization delay
to 14 days.</p>
<p>In addition to that:</p>
<ol class="arabic simple">
<li><p>When stabilizing across a major version bump (e.g. from 1.1.x
to 1.2.x), prefer waiting until the newest minor version becomes
the stable candidate (i.e. do not stabilize from 1.1.4 to 1.2.0
if 1.2.1 is available).  When stabilizing over a minor version bump
(e.g. from 1.1.4 to 1.1.5), feel free to proceed immediately.</p></li>
<li><p>If reverse dependencies block upgrade of a package (e.g. through
<code class="docutils literal notranslate"><span class="pre">&lt;</span></code> dependencies), consider stabilizing the newest versions
matching the restriction as well.  The same is worth considering
if upstream maintains multiple versions simultaneously with major
API changes, even if there are no explicit <code class="docutils literal notranslate"><span class="pre">&lt;</span></code> dependencies
(e.g. <code class="docutils literal notranslate"><span class="pre">dev-python/django</span></code>).</p></li>
<li><p>If a new release is likely to cause major compatibility issues
(e.g. major releases of <code class="docutils literal notranslate"><span class="pre">dev-python/sphinx</span></code>), consider delaying
the stabilization and/or explicitly testing its reverse dependencies,
in order to ensure that necessary <code class="docutils literal notranslate"><span class="pre">&lt;</span></code> dependencies are added first.</p></li>
<li><p>Avoid stabilizing prereleases (alpha, beta and RC versions), unless
it is necessary and upstream does not provide a final release
for a significant time.</p></li>
<li><p>Ideally, aim for tests to pass on all relevant architectures.  Add
deselects if necessary, as this will ensure that future
stabilizations will be handled faster.</p></li>
</ol>
</section>
<section id="tooling">
<h3>Tooling<a class="headerlink" href="#tooling" title="Link to this heading">¶</a></h3>
<p>The recommended way of filing stabilization requests is to use
<code class="docutils literal notranslate"><span class="pre">stablereq-*</span></code> tools from <code class="docutils literal notranslate"><span class="pre">app-portage/mgorny-dev-scripts</span></code> package,
combined with <code class="docutils literal notranslate"><span class="pre">pkgdev</span></code> from <code class="docutils literal notranslate"><span class="pre">dev-util/pkgdev</span></code>.</p>
<p>To prepare the initial stabilization list and open it in an editor:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nb">export</span><span class="w"> </span><span class="nv">PKGCHECK_ARGS</span><span class="o">=</span><span class="s2">&quot;--stabletime 14&quot;</span>
git<span class="w"> </span>grep<span class="w"> </span>-l<span class="w"> </span>python@<span class="w"> </span><span class="s1">&#39;**/metadata.xml&#39;</span><span class="w"> </span><span class="p">|</span>
<span class="w">    </span>cut<span class="w"> </span>-d/<span class="w"> </span>-f1-2<span class="w"> </span><span class="p">|</span>
<span class="w">    </span>grep<span class="w"> </span>-v<span class="w"> </span>dev-python/<span class="w"> </span><span class="p">|</span>
<span class="w">    </span>xargs<span class="w"> </span>stablereq-make-list<span class="w"> </span><span class="s1">&#39;dev-python/*&#39;</span>
</pre></div>
</div>
<p>Simultaneously, the following call can be used to run <code class="docutils literal notranslate"><span class="pre">eshowkw</span></code>
to display current keywords on all stabilization candidates:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nb">export</span><span class="w"> </span><span class="nv">PKGCHECK_ARGS</span><span class="o">=</span><span class="s2">&quot;--stabletime 14&quot;</span>
git<span class="w"> </span>grep<span class="w"> </span>-l<span class="w"> </span>python@<span class="w"> </span><span class="s1">&#39;**/metadata.xml&#39;</span><span class="w"> </span><span class="p">|</span>
<span class="w">    </span>cut<span class="w"> </span>-d/<span class="w"> </span>-f1-2<span class="w"> </span><span class="p">|</span>
<span class="w">    </span>grep<span class="w"> </span>-v<span class="w"> </span>dev-python/<span class="w"> </span><span class="p">|</span>
<span class="w">    </span>xargs<span class="w"> </span>stablereq-eshowkw<span class="w"> </span><span class="s1">&#39;dev-python/*&#39;</span>
</pre></div>
</div>
<p>Edit the list as desirable, save into a file and then feed the file
into pkgdev:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>pkgdev<span class="w"> </span>bugs<span class="w"> </span>--auto-cc-arches<span class="o">=</span>*<span class="w"> </span><span class="k">$(</span>&lt;<span class="s2">&quot;</span><span class="si">${</span><span class="nv">file_path</span><span class="si">}</span><span class="s2">&quot;</span><span class="k">)</span>
</pre></div>
</div>
</section>
</section>
<section id="routine-checks-on-installed-python-packages">
<h2>Routine checks on installed Python packages<a class="headerlink" href="#routine-checks-on-installed-python-packages" title="Link to this heading">¶</a></h2>
<p>The following actions are recommended to be run periodically on systems
used to test Python packages.  They could be run e.g. via post-sync
actions.</p>
<section id="pip-check">
<h3>pip check<a class="headerlink" href="#pip-check" title="Link to this heading">¶</a></h3>
<p><code class="docutils literal notranslate"><span class="pre">pip</span> <span class="pre">check</span></code> (provided by <code class="docutils literal notranslate"><span class="pre">dev-python/pip</span></code>) can be used to check
installed packages for missing dependencies and version conflicts:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>$ python3.10 -m pip check
meson-python 0.6.0 requires ninja, which is not installed.
cx-freeze 6.11.1 requires patchelf, which is not installed.
openapi-spec-validator 0.4.0 has requirement openapi-schema-validator&lt;0.3.0,&gt;=0.2.0, but you have openapi-schema-validator 0.3.0.
cx-freeze 6.11.1 has requirement setuptools&lt;=60.10.0,&gt;=59.0.1, but you have setuptools 62.6.0.
</pre></div>
</div>
<p>This tool checks the installed packages for a single Python
implementation only, so you need to run it for every installed
interpreter separately.</p>
<p>In some cases the issues are caused by unnecessary version pins
or upstream packages listing optional dependencies as obligatory.
The preferred fix is to fix the package metadata rather than modifying
the dependencies in ebuild.</p>
<div class="admonition warning">
<p class="admonition-title">Warning</p>
<p>pip does not support the <code class="docutils literal notranslate"><span class="pre">Provides</span></code> metadata, so it can
produce false positives about <code class="docutils literal notranslate"><span class="pre">certifi</span></code> dependency.  Please ignore
these:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>httpcore 0.15.0 requires certifi, which is not installed.
httpx 0.23.0 requires certifi, which is not installed.
sphobjinv 2.2.2 requires certifi, which is not installed.
requests 2.28.0 requires certifi, which is not installed.
</pre></div>
</div>
</div>
</section>
<section id="pip-list-outdated">
<h3>pip list --outdated<a class="headerlink" href="#pip-list-outdated" title="Link to this heading">¶</a></h3>
<p><code class="docutils literal notranslate"><span class="pre">pip</span> <span class="pre">list</span> <span class="pre">--outdated</span></code> (provided by <code class="docutils literal notranslate"><span class="pre">dev-python/pip</span></code>) can be used
to check whether installed packages are up-to-date.  This can help
checking for pending version bumps, as well as to detect wrong versions
in installed metadata:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>$ pip3.11 list --outdated
Package                  Version           Latest  Type
------------------------ ----------------- ------- -----
dirty-equals             0                 0.4     wheel
filetype                 1.0.10            1.0.13  wheel
mercurial                6.1.3             6.1.4   sdist
node-semver              0.8.0             0.8.1   wheel
PyQt-builder             1.12.2            1.13.0  wheel
PyQt5                    5.15.6            5.15.7  wheel
PyQt5-sip                12.10.1           12.11.0 sdist
PyQtWebEngine            5.15.5            5.15.6  wheel
Routes                   2.5.1.dev20220522 2.5.1   wheel
selenium                 3.141.0           4.3.0   wheel
sip                      6.6.1             6.6.2   wheel
sphinxcontrib-websupport 1.2.4.dev20220515 1.2.4   wheel
uri-template             0.0.0             1.2.0   wheel
watchfiles               0.0.0             0.15.0  wheel
watchgod                 0.0.dev0          0.8.2   wheel
</pre></div>
</div>
<p>Again, the action applies to a single Python implementation only
and needs to be repeated for all of them.</p>
<p>Particularly note the packages with versions containing only zeroes
in the above list — this is usually a sign that the build system
does not recognize the version correctly.  In some cases, the only
working solution would be to sed the correct version in.</p>
<p>The additional <code class="docutils literal notranslate"><span class="pre">dev</span></code> suffix is usually appended via <code class="docutils literal notranslate"><span class="pre">tag_build</span></code>
option in <code class="docutils literal notranslate"><span class="pre">setup.cfg</span></code>.  This causes the version to be considered
older than the actual release, and therefore the respective options need
to be stripped.</p>
</section>
<section id="gpy-verify-deps">
<h3>gpy-verify-deps<a class="headerlink" href="#gpy-verify-deps" title="Link to this heading">¶</a></h3>
<p><code class="docutils literal notranslate"><span class="pre">gpy-verify-deps</span></code> (provided by <code class="docutils literal notranslate"><span class="pre">app-portage/gpyutils</span></code>) compares
the ebuild dependencies of all installed Python packages against their
metadata.  It reports the dependencies that are potentially missing
in ebuilds, as well as dependencies potentially missing
<code class="docutils literal notranslate"><span class="pre">[${PYTHON_USEDEP}]</span></code>.  For the latter, it assumes that all
dependencies listed in package metadata are used as Python modules.</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>$ gpy-verify-deps
[...]
=dev-python/tempest-31.0.0: missing dependency: dev-python/oslo-serialization [*]
=dev-python/tempest-31.0.0: missing dependency: dev-python/cryptography [*]
=dev-python/tempest-31.0.0: missing dependency: dev-python/stestr [*]
=dev-python/versioningit-2.0.0: missing dependency: dev-python/tomli [*]
=dev-python/versioningit-2.0.0: missing dependency: dev-python/importlib_metadata [python3.8 python3.9]
=dev-python/wstools-0.4.10-r1: missing dependency: dev-python/setuptools [*]
</pre></div>
</div>
<p>The check is done for all installed interpreters.  The report indicates
whether the dependency upstream is unconditional (<code class="docutils literal notranslate"><span class="pre">[*]</span></code>) or specific
to a subset of Python implementations.</p>
<p>Similarly to <code class="docutils literal notranslate"><span class="pre">pip</span> <span class="pre">check</span></code> results, every dependency needs to be
verified.  In many cases, upstream metadata lists optional or build-time
dependencies as runtime dependencies, and it is preferable to strip them
than to copy the mistakes into the ebuild.</p>
</section>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Gentoo Python Guide</a></h1>









<search id="searchbox" style="display: none" role="search">
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Search"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</search>
<script>document.getElementById('searchbox').style.display = "block"</script><h3>Navigation</h3>
<p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="preface.html">Preface</a></li>
<li class="toctree-l1"><a class="reference internal" href="interpreter.html">Python interpreters</a></li>
<li class="toctree-l1"><a class="reference internal" href="eclass.html">Choosing between Python eclasses</a></li>
<li class="toctree-l1"><a class="reference internal" href="basic.html">Common basics</a></li>
<li class="toctree-l1"><a class="reference internal" href="any.html">python-any-r1 — build-time dependency</a></li>
<li class="toctree-l1"><a class="reference internal" href="single.html">python-single-r1 — single-impl packages</a></li>
<li class="toctree-l1"><a class="reference internal" href="multi.html">python-r1 — multi-impl packages</a></li>
<li class="toctree-l1"><a class="reference internal" href="distutils.html">distutils-r1 — standard Python build systems</a></li>
<li class="toctree-l1"><a class="reference internal" href="test.html">Tests in Python packages</a></li>
<li class="toctree-l1"><a class="reference internal" href="distutils-legacy.html">distutils-r1 legacy concepts</a></li>
<li class="toctree-l1"><a class="reference internal" href="pypi.html">pypi — helper eclass for PyPI archives</a></li>
<li class="toctree-l1"><a class="reference internal" href="helper.html">Common helper functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="depend.html">Advanced dependencies</a></li>
<li class="toctree-l1"><a class="reference internal" href="pytest.html">pytest recipes</a></li>
<li class="toctree-l1"><a class="reference internal" href="concept.html">Advanced concepts</a></li>
<li class="toctree-l1"><a class="reference internal" href="expert-multi.html">Expert python-r1 usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="buildsys.html">Integration with build systems written in Python</a></li>
<li class="toctree-l1"><a class="reference internal" href="porting.html">Porting tips</a></li>
<li class="toctree-l1"><a class="reference internal" href="migration.html">Migration guides</a></li>
<li class="toctree-l1"><a class="reference internal" href="qawarn.html">QA checks and warnings</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Python package maintenance</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#package-name-policy">Package name policy</a></li>
<li class="toctree-l2"><a class="reference internal" href="#support-for-python-2">Support for Python 2</a></li>
<li class="toctree-l2"><a class="reference internal" href="#which-implementations-to-test-new-packages-for">Which implementations to test new packages for?</a></li>
<li class="toctree-l2"><a class="reference internal" href="#adding-new-python-implementations-to-existing-packages">Adding new Python implementations to existing packages</a></li>
<li class="toctree-l2"><a class="reference internal" href="#which-packages-can-be-co-maintained-by-the-python-project">Which packages can be (co-)maintained by the Python project?</a></li>
<li class="toctree-l2"><a class="reference internal" href="#porting-packages-to-a-new-eapi">Porting packages to a new EAPI</a></li>
<li class="toctree-l2"><a class="reference internal" href="#monitoring-new-package-versions">Monitoring new package versions</a></li>
<li class="toctree-l2"><a class="reference internal" href="#stabilization-recommendations">Stabilization recommendations</a></li>
<li class="toctree-l2"><a class="reference internal" href="#routine-checks-on-installed-python-packages">Routine checks on installed Python packages</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="interpreter-maintenance.html">Maintenance of Python implementations</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="qawarn.html" title="previous chapter">QA checks and warnings</a></li>
      <li>Next: <a href="interpreter-maintenance.html" title="next chapter">Maintenance of Python implementations</a></li>
  </ul></li>
</ul>
</div>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &#169;2020, Michał Górny, license: CC BY 4.0.
      
      |
      Powered by <a href="https://www.sphinx-doc.org/">Sphinx 8.1.3</a>
      &amp; <a href="https://alabaster.readthedocs.io">Alabaster 1.0.0</a>
      
      |
      <a href="_sources/package-maintenance.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>