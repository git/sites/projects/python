<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>pytest recipes &#8212; Gentoo Python Guide  documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=d1102ebc" />
    <link rel="stylesheet" type="text/css" href="_static/basic.css?v=686e5160" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css?v=27fed22d" />
    <script src="_static/documentation_options.js?v=5929fcd5"></script>
    <script src="_static/doctools.js?v=9bcbadda"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Advanced concepts" href="concept.html" />
    <link rel="prev" title="Advanced dependencies" href="depend.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  

  
  

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="pytest-recipes">
<h1>pytest recipes<a class="headerlink" href="#pytest-recipes" title="Link to this heading">¶</a></h1>
<section id="skipping-tests-based-on-markers">
<h2>Skipping tests based on markers<a class="headerlink" href="#skipping-tests-based-on-markers" title="Link to this heading">¶</a></h2>
<p>A few packages use <a class="reference external" href="https://docs.pytest.org/en/stable/example/markers.html">custom pytest markers</a> to indicate e.g. tests
requiring Internet access.  These markers can be used to conveniently
disable whole test groups, e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>python_test<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span>epytest<span class="w"> </span>-m<span class="w"> </span><span class="s1">&#39;not network&#39;</span><span class="w"> </span>dask
<span class="o">}</span>
</pre></div>
</div>
</section>
<section id="skipping-tests-based-on-paths-names">
<h2>Skipping tests based on paths/names<a class="headerlink" href="#skipping-tests-based-on-paths-names" title="Link to this heading">¶</a></h2>
<p>There are two primary methods of skipping tests based on path (and name)
in pytest: using <code class="docutils literal notranslate"><span class="pre">--ignore</span></code> and <code class="docutils literal notranslate"><span class="pre">--deselect</span></code>.</p>
<p><code class="docutils literal notranslate"><span class="pre">--ignore</span></code> causes pytest to entirely ignore a file or a directory
when collecting tests.  This works only for skipping whole files but it
ignores missing dependencies and other failures occurring while
importing the test file.</p>
<p><code class="docutils literal notranslate"><span class="pre">--deselect</span></code> causes pytest to skip the specific test or tests.  It can
be also used to select individual tests or even parametrized variants
of tests.</p>
<p>Both options can be combined to get tests to pass without having
to alter the test files.  They are preferable over suggestions from
skipping problematic tests when tests are installed as part
of the package.  They can also be easily applied conditionally to Python
interpreter.</p>
<p>The modern versions of eclasses provide two control variables,
<code class="docutils literal notranslate"><span class="pre">EPYTEST_IGNORE</span></code> and <code class="docutils literal notranslate"><span class="pre">EPYTEST_DESELECT</span></code> that can be used to list
test files or tests to be ignored or deselected respectively.  These
variables can be used in global scope to avoid redefining
<code class="docutils literal notranslate"><span class="pre">python_test()</span></code>.  However, combining them with additional conditions
requires using the local scope.</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>python_test<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span><span class="nb">local</span><span class="w"> </span><span class="nv">EPYTEST_IGNORE</span><span class="o">=(</span>
<span class="w">        </span><span class="c1"># ignore whole file with missing dep</span>
<span class="w">        </span>tests/test_client.py
<span class="w">    </span><span class="o">)</span>
<span class="w">    </span><span class="nb">local</span><span class="w"> </span><span class="nv">EPYTEST_DESELECT</span><span class="o">=(</span>
<span class="w">        </span><span class="c1"># deselect a single test</span>
<span class="w">        </span><span class="s1">&#39;tests/utils/test_general.py::test_filename&#39;</span>
<span class="w">        </span><span class="c1"># deselect a parametrized test based on first param</span>
<span class="w">        </span><span class="s1">&#39;tests/test_transport.py::test_transport_works[eventlet&#39;</span>
<span class="w">    </span><span class="o">)</span>
<span class="w">    </span><span class="o">[[</span><span class="w"> </span><span class="si">${</span><span class="nv">EPYTHON</span><span class="si">}</span><span class="w"> </span><span class="o">==</span><span class="w"> </span>python3.6<span class="w"> </span><span class="o">]]</span><span class="w"> </span><span class="o">&amp;&amp;</span><span class="w"> </span><span class="nv">EPYTEST_DESELECT</span><span class="o">+=(</span>
<span class="w">        </span><span class="c1"># deselect a test for py3.6 only</span>
<span class="w">        </span><span class="s1">&#39;tests/utils/test_contextvars.py::test_leaks[greenlet]&#39;</span>
<span class="w">    </span><span class="o">)</span>
<span class="w">    </span>epytest
<span class="o">}</span>
</pre></div>
</div>
</section>
<section id="avoiding-the-dependency-on-pytest-runner">
<h2>Avoiding the dependency on pytest-runner<a class="headerlink" href="#avoiding-the-dependency-on-pytest-runner" title="Link to this heading">¶</a></h2>
<p><a class="reference external" href="https://pypi.org/project/pytest-runner/">pytest-runner</a> is a package providing <code class="docutils literal notranslate"><span class="pre">pytest</span></code> command to setuptools.
While it might be convenient upstream, there is no real reason to use
it in Gentoo packages.  It has no real advantage over calling pytest
directly.</p>
<p>Some packages declare the dependency on <code class="docutils literal notranslate"><span class="pre">pytest-runner</span></code>
in <code class="docutils literal notranslate"><span class="pre">setup_requires</span></code>.  As a result, the dependency is enforced whenever
<code class="docutils literal notranslate"><span class="pre">setup.py</span></code> is being run, even if the user has no intention of running
tests.  If this is the case, the dependency must be stripped.</p>
<p>The recommended method of stripping it is to use sed:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>python_prepare_all<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span>sed<span class="w"> </span>-i<span class="w"> </span>-e<span class="w"> </span><span class="s1">&#39;/pytest-runner/d&#39;</span><span class="w"> </span>setup.py<span class="w"> </span><span class="o">||</span><span class="w"> </span>die
<span class="w">    </span>distutils-r1_python_prepare_all
<span class="o">}</span>
</pre></div>
</div>
</section>
<section id="disabling-plugin-autoloading">
<span id="index-1"></span><span id="index-0"></span><h2>Disabling plugin autoloading<a class="headerlink" href="#disabling-plugin-autoloading" title="Link to this heading">¶</a></h2>
<p>Normally, when running a test suite pytest loads all plugins installed
on the system.  This is often convenient for upstreams, as it makes it
possible to use the features provided by the plugins (such as <code class="docutils literal notranslate"><span class="pre">async</span></code>
test function support, or fixtures) without the necessity to explicitly
enable them.  However, there are also cases when additional plugins
could make the test suite fail or become very slow (especially if pytest
is called recursively).</p>
<p>The modern recommendation for these cases is to disable plugin
autoloading via setting the <code class="docutils literal notranslate"><span class="pre">PYTEST_DISABLE_PLUGIN_AUTOLOAD</span></code>
environment variable, and then explicitly enable specific plugins
if necessary.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>Previously we used to recommend explicitly disabling problematic
plugins via <code class="docutils literal notranslate"><span class="pre">-p</span> <span class="pre">no:&lt;plugin&gt;</span></code>.  However, it is rarely obvious
which plugin is causing the problems, and it is entirely possible
that another plugin will cause issues in the future, so an opt-in
approach is usually faster and more reliable.</p>
</div>
<p>The easier approach to enabling plugins is to use the <code class="docutils literal notranslate"><span class="pre">-p</span></code> option,
listing specific plugins.  The option can be passed multiple times,
and accepts a plugin name as specified in the package’s
<code class="docutils literal notranslate"><span class="pre">entry_points.txt</span></code> file:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>python_test<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span><span class="nb">local</span><span class="w"> </span>-x<span class="w"> </span><span class="nv">PYTEST_DISABLE_PLUGIN_AUTOLOAD</span><span class="o">=</span><span class="m">1</span>
<span class="w">    </span>epytest<span class="w"> </span>-p<span class="w"> </span>asyncio<span class="w"> </span>-p<span class="w"> </span>tornado
<span class="o">}</span>
</pre></div>
</div>
<p>However, this approach does not work when the test suite calls pytest
recursively (e.g. you are testing a pytest plugin).  In this case,
the <code class="docutils literal notranslate"><span class="pre">PYTEST_PLUGINS</span></code> environment variable can be used instead.  It
takes a comma-separated list of plugin <em>module names</em>:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>python_test<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span><span class="nb">local</span><span class="w"> </span>-x<span class="w"> </span><span class="nv">PYTEST_DISABLE_PLUGIN_AUTOLOAD</span><span class="o">=</span><span class="m">1</span>
<span class="w">    </span><span class="nb">local</span><span class="w"> </span>-x<span class="w"> </span><span class="nv">PYTEST_PLUGINS</span><span class="o">=</span>xdist.plugin,xdist.looponfail,pytest_forked

<span class="w">    </span>epytest
<span class="o">}</span>
</pre></div>
</div>
<p>Please note that failing to enable all the required plugins may cause
some of the tests to be skipped implicitly (especially if the test suite
is using <code class="docutils literal notranslate"><span class="pre">async</span></code> functions and no async plugin is loaded).  Please
look at skip messages and warnings to make sure everything works
as intended.</p>
</section>
<section id="using-pytest-xdist-to-run-tests-in-parallel">
<span id="index-2"></span><h2>Using pytest-xdist to run tests in parallel<a class="headerlink" href="#using-pytest-xdist-to-run-tests-in-parallel" title="Link to this heading">¶</a></h2>
<p><a class="reference external" href="https://pypi.org/project/pytest-xdist/">pytest-xdist</a> is a plugin that makes it possible to run multiple tests
in parallel.  This is especially useful for programs with large test
suites that take significant time to run single-threaded.</p>
<p>Using pytest-xdist is recommended if the package in question supports it
(i.e. it does not cause semi-random test failures) and its test suite
takes significant time.  This is done via setting <code class="docutils literal notranslate"><span class="pre">EPYTEST_XDIST</span></code>
to a non-empty value prior to calling <code class="docutils literal notranslate"><span class="pre">distutils_enable_tests</span></code>.
It ensures that an appropriate depedency is added, and that <code class="docutils literal notranslate"><span class="pre">epytest</span></code>
adds necessary command-line options.</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nv">EPYTEST_XDIST</span><span class="o">=</span><span class="m">1</span>
distutils_enable_tests<span class="w"> </span>pytest
</pre></div>
</div>
<p>Please note that some upstream use pytest-xdist even if there is no real
gain from doing so.  If the package’s tests take a short time to finish,
please avoid the dependency and strip it if necessary.</p>
<p>Not all test suites support pytest-xdist.  Particularly, it requires
that the tests are written not to collide one with another.  Sometimes,
xdist may also cause instability of individual tests.  In some cases,
it is possible to work around this using the same solution as when
<a class="reference internal" href="#dealing-with-flaky-tests">dealing with flaky tests</a>.</p>
<p>When only a few tests are broken or unstable because of pytest-xdist,
it is possible to use it and deselect the problematic tests.  It is up
to the maintainer’s discretion to decide whether this is justified.</p>
</section>
<section id="dealing-with-flaky-tests">
<h2>Dealing with flaky tests<a class="headerlink" href="#dealing-with-flaky-tests" title="Link to this heading">¶</a></h2>
<p>A flaky test is a test that sometimes passes, and sometimes fails
with a false positive result.  Often tests are flaky because of too
steep timing requirements or race conditions.  While generally it is
preferable to fix the underlying issue (e.g. by increasing timeouts),
it is not always easy.</p>
<p>Sometimes upstreams use such packages as <code class="docutils literal notranslate"><span class="pre">dev-python/flaky</span></code>
or <code class="docutils literal notranslate"><span class="pre">dev-python/pytest-rerunfailures</span></code> to mark tests as flaky and have
them rerun a few minutes automatically.  If upstream does not do that,
it is also possible to force a similar behavior locally in the ebuild:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>python_test<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span><span class="c1"># plugins make tests slower, and more fragile</span>
<span class="w">    </span><span class="nb">local</span><span class="w"> </span>-x<span class="w"> </span><span class="nv">PYTEST_DISABLE_PLUGIN_AUTOLOAD</span><span class="o">=</span><span class="m">1</span>
<span class="w">    </span><span class="c1"># some tests are very fragile to timing</span>
<span class="w">    </span>epytest<span class="w"> </span>-p<span class="w"> </span>rerunfailures<span class="w"> </span>--reruns<span class="o">=</span><span class="m">10</span><span class="w"> </span>--reruns-delay<span class="o">=</span><span class="m">2</span>
<span class="o">}</span>
</pre></div>
</div>
<p>Note that the snippet above also disables plugin autoloading to speed
tests up and therefore reduce their flakiness.  Sometimes forcing
explicit rerun also makes it possible to use xdist on packages that
otherwise randomly fail with it.</p>
</section>
<section id="using-pytest-timeout-to-prevent-deadlocks-hangs">
<span id="index-3"></span><h2>Using pytest-timeout to prevent deadlocks (hangs)<a class="headerlink" href="#using-pytest-timeout-to-prevent-deadlocks-hangs" title="Link to this heading">¶</a></h2>
<p><a class="reference external" href="https://pypi.org/project/pytest-timeout/">pytest-timeout</a> plugin adds an option to terminate the test if its
runtime exceeds the specified limit.  Some packages decorate specific
tests with timeouts; however, it is also possible to set a baseline
timeout for all tests.</p>
<p>A timeout causes the test run to fail, and therefore using it is
not generally necessary for test suites that are working correctly.
If individual tests are known to suffer from unfixable hangs, it is
preferable to deselect them.  However, setting a general timeout is
recommended when a package is particularly fragile, or has suffered
deadlocks in the past.  A proactive setting can prevent it from hanging
and blocking arch testing machines.</p>
<p>The plugin can be enabled via setting <code class="docutils literal notranslate"><span class="pre">EPYTEST_TIMEOUT</span></code> to the timeout
in seconds, prior to calling <code class="docutils literal notranslate"><span class="pre">distutils_enable_tests</span></code>.  This ensures
that an appropriate depedency is added, and that <code class="docutils literal notranslate"><span class="pre">epytest</span></code> adds
necessary command-line options.</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>:<span class="w"> </span><span class="si">${</span><span class="nv">EPYTEST_TIMEOUT</span><span class="p">:=1800</span><span class="si">}</span>
distutils_enable_tests<span class="w"> </span>pytest
</pre></div>
</div>
<p>The timeout applies to every test separately, i.e. the above example
will cause a single test to time out after 30 minutes.  If multiple
tests hang, the total run time will multiply consequently.</p>
<p>When deciding on a timeout value, please take into the consideration
that the tests may be run on a low performance hardware, and on a busy
system, and choose an appropriately high value.</p>
<p>It is a good idea to use the default assignment form, as in the snippet
above, as that permits the user to easily override the timeout
if necessary.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p><code class="docutils literal notranslate"><span class="pre">EPYTEST_TIMEOUT</span></code> can also be set by user in <code class="docutils literal notranslate"><span class="pre">make.conf</span></code>
or in the calling environment.  This can be used as a general
protection against hanging test suites.  However, please note that
this does not control dependencies, and therefore the user may need
to install <code class="docutils literal notranslate"><span class="pre">dev-python/pytest-timeout</span></code> explicitly.</p>
</div>
</section>
<section id="avoiding-dependencies-on-other-pytest-plugins">
<h2>Avoiding dependencies on other pytest plugins<a class="headerlink" href="#avoiding-dependencies-on-other-pytest-plugins" title="Link to this heading">¶</a></h2>
<p>There is a number of pytest plugins that have little value to Gentoo
users.  They include plugins for test coverage
(<code class="docutils literal notranslate"><span class="pre">dev-python/pytest-cov</span></code>), coding style (<code class="docutils literal notranslate"><span class="pre">dev-python/pytest-flake8</span></code>)
and more.  Generally, packages should avoid using those plugins.</p>
<div class="admonition warning">
<p class="admonition-title">Warning</p>
<p>As of 2022-01-24, <code class="docutils literal notranslate"><span class="pre">epytest</span></code> disables a few undesirable plugins
by default.  As a result, developers have a good chance
of experiencing failures due to hardcoded pytest options first,
even if they have the relevant plugins installed.</p>
<p>If your package <em>really</em> needs to use the specific plugin, you need
to pass <code class="docutils literal notranslate"><span class="pre">-p</span> <span class="pre">&lt;plugin&gt;</span></code> explicitly to reenable it.</p>
</div>
<p>In some cases, upstream packages only list them as dependencies
but do not use them automatically.  In other cases, you will need
to strip options enabling them from <code class="docutils literal notranslate"><span class="pre">pytest.ini</span></code> or <code class="docutils literal notranslate"><span class="pre">setup.cfg</span></code>.</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>src_prepare<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span>sed<span class="w"> </span>-i<span class="w"> </span>-e<span class="w"> </span><span class="s1">&#39;s:--cov=wheel::&#39;</span><span class="w"> </span>setup.cfg<span class="w"> </span><span class="o">||</span><span class="w"> </span>die
<span class="w">    </span>distutils-r1_src_prepare
<span class="o">}</span>
</pre></div>
</div>
</section>
<section id="typeerror-make-test-flaky-got-an-unexpected-keyword-argument-reruns">
<h2>TypeError: _make_test_flaky() got an unexpected keyword argument ‘reruns’<a class="headerlink" href="#typeerror-make-test-flaky-got-an-unexpected-keyword-argument-reruns" title="Link to this heading">¶</a></h2>
<p>If you see a test error resembling the following:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>TypeError:<span class="w"> </span>_make_test_flaky<span class="o">()</span><span class="w"> </span>got<span class="w"> </span>an<span class="w"> </span>unexpected<span class="w"> </span>keyword<span class="w"> </span>argument<span class="w"> </span><span class="s1">&#39;reruns&#39;</span>
</pre></div>
</div>
<p>This means that the tests are being run via <a class="reference external" href="https://github.com/box/flaky/">flaky</a> plugin while
the package in question expects <a class="reference external" href="https://github.com/pytest-dev/pytest-rerunfailures/">pytest-rerunfailures</a>.  This is
because both plugins utilize the same <code class="docutils literal notranslate"><span class="pre">&#64;pytest.mark.flaky</span></code> marker
but support different set of arguments.</p>
<p>To resolve the problem, explicitly disable the <code class="docutils literal notranslate"><span class="pre">flaky</span></code> plugin and make
sure to depend on <code class="docutils literal notranslate"><span class="pre">dev-python/pytest-rerunfailures</span></code>:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nv">BDEPEND</span><span class="o">=</span><span class="s2">&quot;</span>
<span class="s2">    test? (</span>
<span class="s2">         dev-python/pytest-rerunfailures[</span><span class="si">${</span><span class="nv">PYTHON_USEDEP</span><span class="si">}</span><span class="s2">]</span>
<span class="s2">    )&quot;</span>

python_test<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span>epytest<span class="w"> </span>-p<span class="w"> </span>no:flaky
<span class="o">}</span>
</pre></div>
</div>
</section>
<section id="importpathmismatcherror">
<h2>ImportPathMismatchError<a class="headerlink" href="#importpathmismatcherror" title="Link to this heading">¶</a></h2>
<p>An <code class="docutils literal notranslate"><span class="pre">ImportPathMismatchError</span></code> generally indicates that the same Python
module (or one that supposedly looks the same) has been loaded twice
using different paths, e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>E<span class="w">   </span>_pytest.pathlib.ImportPathMismatchError:<span class="w"> </span><span class="o">(</span><span class="s1">&#39;path&#39;</span>,<span class="w"> </span><span class="s1">&#39;/usr/lib/pypy3.7/site-packages/path&#39;</span>,<span class="w"> </span>PosixPath<span class="o">(</span><span class="s1">&#39;/tmp/portage/dev-python/jaraco-path-3.3.1/work/jaraco.path-3.3.1/jaraco/path.py&#39;</span><span class="o">))</span>
</pre></div>
</div>
<p>These problems are usually caused by pytest test discovery getting
confused by namespace packages.  In this case, the <code class="docutils literal notranslate"><span class="pre">jaraco</span></code> directory
is a Python 3-style namespace but pytest is treating it as a potential
test directory.  Therefore, instead of loading it as <code class="docutils literal notranslate"><span class="pre">jaraco.path</span></code>
relatively to the top directory, it loads it as <code class="docutils literal notranslate"><span class="pre">path</span></code> relatively
to the <code class="docutils literal notranslate"><span class="pre">jaraco</span></code> directory.</p>
<p>The simplest way to resolve this problem is to restrict the test
discovery to the actual test directories, e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>python_test<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span>epytest<span class="w"> </span><span class="nb">test</span>
<span class="o">}</span>
</pre></div>
</div>
<p>or:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>python_test<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span>epytest<span class="w"> </span>--ignore<span class="w"> </span>jaraco
<span class="o">}</span>
</pre></div>
</div>
</section>
<section id="failures-due-to-missing-files-in-temporary-directories">
<h2>Failures due to missing files in temporary directories<a class="headerlink" href="#failures-due-to-missing-files-in-temporary-directories" title="Link to this heading">¶</a></h2>
<p>As of 2024-01-05, <code class="docutils literal notranslate"><span class="pre">epytest</span></code> overrides the default temporary directory
retention policy of pytest.  By default, directories from successful
tests are removed immediately, and the temporary directories
from the previous test run are replaced by the subsequent test run.
This frequently reduces disk space requirements from test suites,
but it can rarely cause tests to fail.</p>
<p>If you notice test failures combined with indications that a file was
not found, and especially regarding the pytest temporary directories,
try if overriding the retention policy helps, e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>python_test<span class="o">()</span><span class="w"> </span><span class="o">{</span>
<span class="w">    </span>epytest<span class="w"> </span>-o<span class="w"> </span><span class="nv">tmp_path_retention_policy</span><span class="o">=</span>all
<span class="o">}</span>
</pre></div>
</div>
</section>
<section id="fixture-not-found">
<h2>fixture ‘…’ not found<a class="headerlink" href="#fixture-not-found" title="Link to this heading">¶</a></h2>
<p>Most of the time, a missing fixture indicates that some pytest plugin
is not installed.  In rare cases, it can signify an incompatible pytest
version or package issue.</p>
<p>The following table maps common fixture names to their respective
plugins.</p>
<table class="docutils align-default">
<thead>
<tr class="row-odd"><th class="head"><p>Fixture name</p></th>
<th class="head"><p>Package</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>event_loop</p></td>
<td><p>dev-python/pytest-asyncio</p></td>
</tr>
<tr class="row-odd"><td><p>freezer</p></td>
<td><p>dev-python/pytest-freezegun</p></td>
</tr>
<tr class="row-even"><td><p>httpbin</p></td>
<td><p>dev-python/pytest-httpbin</p></td>
</tr>
<tr class="row-odd"><td><p>loop</p></td>
<td><p>dev-python/pytest-aiohttp</p></td>
</tr>
<tr class="row-even"><td><p>mocker</p></td>
<td><p>dev-python/pytest-mock</p></td>
</tr>
</tbody>
</table>
</section>
<section id="warnings-and-pytest-raises">
<span id="index-6"></span><span id="index-5"></span><span id="index-4"></span><h2>Warnings and <code class="docutils literal notranslate"><span class="pre">pytest.raises()</span></code><a class="headerlink" href="#warnings-and-pytest-raises" title="Link to this heading">¶</a></h2>
<p>Some projects set pytest to raise on warnings, using options such as:</p>
<div class="highlight-toml notranslate"><div class="highlight"><pre><span></span><span class="n">filterwarnings</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="p">[</span>
<span class="w">    </span><span class="s2">&quot;error&quot;</span><span class="p">,</span>
<span class="w">    </span><span class="c1"># ...</span>
<span class="p">]</span>
</pre></div>
</div>
<p>This may be desirable for upstream CI systems, as it ensures that pull
requests do not introduce new warnings, and that any deprecations are
handled promptly.  However, it is undesirable for downstream testing,
as new deprecations in dependencies can cause the existing versions
to start failing.</p>
<p>To avoid this problem, <code class="docutils literal notranslate"><span class="pre">epytest</span></code> explicitly forces <code class="docutils literal notranslate"><span class="pre">-Wdefault</span></code>.
Most of the time, this does not cause any issues, besides causing pytest
to verbosely report warnings that are normally ignored by the test
suite.  However, if some packages incorrectly use <code class="docutils literal notranslate"><span class="pre">pytest.raises()</span></code>
to check for warnings, their test suites will fail, for example:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="o">=============================</span><span class="w"> </span><span class="nv">FAILURES</span><span class="w"> </span><span class="o">=============================</span>
________________<span class="w"> </span>test_ser_ip_with_unexpected_value<span class="w"> </span>_________________

<span class="w">    </span>def<span class="w"> </span>test_ser_ip_with_unexpected_value<span class="o">()</span><span class="w"> </span>-&gt;<span class="w"> </span>None:
<span class="w">        </span><span class="nv">ta</span><span class="w"> </span><span class="o">=</span><span class="w"> </span>TypeAdapter<span class="o">(</span>ipaddress.IPv4Address<span class="o">)</span>

&gt;<span class="w">       </span>with<span class="w"> </span>pytest.raises<span class="o">(</span>UserWarning,<span class="w"> </span><span class="nv">match</span><span class="o">=</span><span class="s1">&#39;serialized value may not be as expected.&#39;</span><span class="o">)</span>:
E<span class="w">       </span>Failed:<span class="w"> </span>DID<span class="w"> </span>NOT<span class="w"> </span>RAISE<span class="w"> </span>&lt;class<span class="w"> </span><span class="s1">&#39;UserWarning&#39;</span>&gt;

tests/test_types.py:6945:<span class="w"> </span><span class="nv">Failed</span>
<span class="o">=========================</span><span class="w"> </span>warnings<span class="w"> </span><span class="nv">summary</span><span class="w"> </span><span class="o">=========================</span>
tests/test_types.py::test_ser_ip_with_unexpected_value
<span class="w">  </span>/tmp/pydantic/pydantic/type_adapter.py:458:<span class="w"> </span>UserWarning:<span class="w"> </span>Pydantic<span class="w"> </span>serializer<span class="w"> </span>warnings:
<span class="w">    </span>PydanticSerializationUnexpectedValue<span class="o">(</span>Expected<span class="w"> </span><span class="sb">`</span>&lt;class<span class="w"> </span><span class="s1">&#39;ipaddress.IPv4Address&#39;</span>&gt;<span class="sb">`</span><span class="w"> </span>but<span class="w"> </span>got<span class="w"> </span><span class="sb">`</span>&lt;class<span class="w"> </span><span class="s1">&#39;int&#39;</span>&gt;<span class="sb">`</span><span class="w"> </span>with<span class="w"> </span>value<span class="w"> </span><span class="sb">`</span><span class="s1">&#39;123&#39;</span><span class="sb">`</span><span class="w"> </span>-<span class="w"> </span>serialized<span class="w"> </span>value<span class="w"> </span>may<span class="w"> </span>not<span class="w"> </span>be<span class="w"> </span>as<span class="w"> </span>expected.<span class="o">)</span>
<span class="w">    </span><span class="k">return</span><span class="w"> </span>self.serializer.to_python<span class="o">(</span>

--<span class="w"> </span>Docs:<span class="w"> </span>https://docs.pytest.org/en/stable/how-to/capture-warnings.html
</pre></div>
</div>
<p>The solution is to replace <code class="docutils literal notranslate"><span class="pre">pytest.raises()</span></code> with more correct
<code class="docutils literal notranslate"><span class="pre">pytest.warns()</span></code>.  The latter will work correctly, independently
of <code class="docutils literal notranslate"><span class="pre">filterwarnings</span></code> value.</p>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Gentoo Python Guide</a></h1>









<search id="searchbox" style="display: none" role="search">
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Search"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</search>
<script>document.getElementById('searchbox').style.display = "block"</script><h3>Navigation</h3>
<p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="preface.html">Preface</a></li>
<li class="toctree-l1"><a class="reference internal" href="interpreter.html">Python interpreters</a></li>
<li class="toctree-l1"><a class="reference internal" href="eclass.html">Choosing between Python eclasses</a></li>
<li class="toctree-l1"><a class="reference internal" href="basic.html">Common basics</a></li>
<li class="toctree-l1"><a class="reference internal" href="any.html">python-any-r1 — build-time dependency</a></li>
<li class="toctree-l1"><a class="reference internal" href="single.html">python-single-r1 — single-impl packages</a></li>
<li class="toctree-l1"><a class="reference internal" href="multi.html">python-r1 — multi-impl packages</a></li>
<li class="toctree-l1"><a class="reference internal" href="distutils.html">distutils-r1 — standard Python build systems</a></li>
<li class="toctree-l1"><a class="reference internal" href="test.html">Tests in Python packages</a></li>
<li class="toctree-l1"><a class="reference internal" href="distutils-legacy.html">distutils-r1 legacy concepts</a></li>
<li class="toctree-l1"><a class="reference internal" href="pypi.html">pypi — helper eclass for PyPI archives</a></li>
<li class="toctree-l1"><a class="reference internal" href="helper.html">Common helper functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="depend.html">Advanced dependencies</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">pytest recipes</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#skipping-tests-based-on-markers">Skipping tests based on markers</a></li>
<li class="toctree-l2"><a class="reference internal" href="#skipping-tests-based-on-paths-names">Skipping tests based on paths/names</a></li>
<li class="toctree-l2"><a class="reference internal" href="#avoiding-the-dependency-on-pytest-runner">Avoiding the dependency on pytest-runner</a></li>
<li class="toctree-l2"><a class="reference internal" href="#disabling-plugin-autoloading">Disabling plugin autoloading</a></li>
<li class="toctree-l2"><a class="reference internal" href="#using-pytest-xdist-to-run-tests-in-parallel">Using pytest-xdist to run tests in parallel</a></li>
<li class="toctree-l2"><a class="reference internal" href="#dealing-with-flaky-tests">Dealing with flaky tests</a></li>
<li class="toctree-l2"><a class="reference internal" href="#using-pytest-timeout-to-prevent-deadlocks-hangs">Using pytest-timeout to prevent deadlocks (hangs)</a></li>
<li class="toctree-l2"><a class="reference internal" href="#avoiding-dependencies-on-other-pytest-plugins">Avoiding dependencies on other pytest plugins</a></li>
<li class="toctree-l2"><a class="reference internal" href="#typeerror-make-test-flaky-got-an-unexpected-keyword-argument-reruns">TypeError: _make_test_flaky() got an unexpected keyword argument ‘reruns’</a></li>
<li class="toctree-l2"><a class="reference internal" href="#importpathmismatcherror">ImportPathMismatchError</a></li>
<li class="toctree-l2"><a class="reference internal" href="#failures-due-to-missing-files-in-temporary-directories">Failures due to missing files in temporary directories</a></li>
<li class="toctree-l2"><a class="reference internal" href="#fixture-not-found">fixture ‘…’ not found</a></li>
<li class="toctree-l2"><a class="reference internal" href="#warnings-and-pytest-raises">Warnings and <code class="docutils literal notranslate"><span class="pre">pytest.raises()</span></code></a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="concept.html">Advanced concepts</a></li>
<li class="toctree-l1"><a class="reference internal" href="expert-multi.html">Expert python-r1 usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="buildsys.html">Integration with build systems written in Python</a></li>
<li class="toctree-l1"><a class="reference internal" href="porting.html">Porting tips</a></li>
<li class="toctree-l1"><a class="reference internal" href="migration.html">Migration guides</a></li>
<li class="toctree-l1"><a class="reference internal" href="qawarn.html">QA checks and warnings</a></li>
<li class="toctree-l1"><a class="reference internal" href="package-maintenance.html">Python package maintenance</a></li>
<li class="toctree-l1"><a class="reference internal" href="interpreter-maintenance.html">Maintenance of Python implementations</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="depend.html" title="previous chapter">Advanced dependencies</a></li>
      <li>Next: <a href="concept.html" title="next chapter">Advanced concepts</a></li>
  </ul></li>
</ul>
</div>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &#169;2020, Michał Górny, license: CC BY 4.0.
      
      |
      Powered by <a href="https://www.sphinx-doc.org/">Sphinx 8.1.3</a>
      &amp; <a href="https://alabaster.readthedocs.io">Alabaster 1.0.0</a>
      
      |
      <a href="_sources/pytest.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>