<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>pypi — helper eclass for PyPI archives &#8212; Gentoo Python Guide  documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=d1102ebc" />
    <link rel="stylesheet" type="text/css" href="_static/basic.css?v=686e5160" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css?v=27fed22d" />
    <script src="_static/documentation_options.js?v=5929fcd5"></script>
    <script src="_static/doctools.js?v=9bcbadda"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Common helper functions" href="helper.html" />
    <link rel="prev" title="distutils-r1 legacy concepts" href="distutils-legacy.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  

  
  

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="pypi-helper-eclass-for-pypi-archives">
<h1>pypi — helper eclass for PyPI archives<a class="headerlink" href="#pypi-helper-eclass-for-pypi-archives" title="Link to this heading">¶</a></h1>
<p>The <code class="docutils literal notranslate"><span class="pre">pypi</span></code> eclass is a small eclass to facilitate fetching sources
from PyPI.  It abstracts away the complexity of PyPI URLs, and makes it
easier to adapt <code class="docutils literal notranslate"><span class="pre">SRC_URI</span></code> to their future changes.</p>
<p>Please note that PyPI archives are not always the best choice
for distfiles.  In particular, they frequently are missing tests
and other files important to Gentoo packaging.  Should that be the case,
other archives should be used.  Read the <a class="reference internal" href="distutils.html#source-archives"><span class="std std-ref">Source archives</span></a> section
for more information.</p>
<p>Eclass reference: <a class="reference external" href="https://devmanual.gentoo.org/eclass-reference/pypi.eclass/index.html">pypi.eclass(5)</a></p>
<section id="pypi-urls">
<h2>PyPI URLs<a class="headerlink" href="#pypi-urls" title="Link to this heading">¶</a></h2>
<section id="modern-and-legacy-urls">
<h3>Modern and legacy URLs<a class="headerlink" href="#modern-and-legacy-urls" title="Link to this heading">¶</a></h3>
<p>The modern form of PyPI URLs include a hash of the distribution file,
e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>https://files.pythonhosted.org/packages/20/2e/36e46173a288c1c40853ffdb712c67e0e022df0e1ce50b7b1b50066b74d4/gpep517-13.tar.gz
</pre></div>
</div>
<p>This makes them inconvenient for use in ebuilds, as the hash would have
to be updated manually on every version bump.  For this reason, Gentoo
settled on using the legacy form of URLs instead, e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>https://files.pythonhosted.org/packages/source/g/gpep517/gpep517-13.tar.gz
</pre></div>
</div>
<p>It should be noted that legacy URLs are no longer supported and may stop
working at an arbitrary time.  Using the eclass is going to make
the migration to an alternative provider easier than inline URLs.</p>
<p>The path part of a legacy URL for a source package has the general form
of:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>/packages/source/<span class="si">${</span><span class="nv">project</span><span class="p">::</span><span class="nv">1</span><span class="si">}</span>/<span class="si">${</span><span class="nv">project</span><span class="si">}</span>/<span class="si">${</span><span class="nv">filename</span><span class="si">}</span>
</pre></div>
</div>
<p>whereas the path for a binary package (wheel) is:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>/packages/<span class="si">${</span><span class="nv">pytag</span><span class="si">}</span>/<span class="si">${</span><span class="nv">project</span><span class="p">::</span><span class="nv">1</span><span class="si">}</span>/<span class="si">${</span><span class="nv">project</span><span class="si">}</span>/<span class="si">${</span><span class="nv">filename</span><span class="si">}</span>
</pre></div>
</div>
<p>In both cases, <code class="docutils literal notranslate"><span class="pre">${project}</span></code> refers to the PyPI project name.
Technically, PyPI accepts any name that matches the PyPI project name
after <a class="reference external" href="https://packaging.python.org/en/latest/specifications/name-normalization/">package name normalization</a>.  However, this behavior is not
guaranteed and using the canonical project name is recommended.</p>
<p>The filenames and <code class="docutils literal notranslate"><span class="pre">${pytag}</span></code> are described in the subsequent sections.</p>
</section>
<section id="source-distribution-filenames">
<h3>Source distribution filenames<a class="headerlink" href="#source-distribution-filenames" title="Link to this heading">¶</a></h3>
<p>The filename of a source distribution (sdist) has the general form of:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="si">${</span><span class="nv">name</span><span class="si">}</span>-<span class="si">${</span><span class="nv">version</span><span class="si">}</span>.tar.gz
</pre></div>
</div>
<p>where <code class="docutils literal notranslate"><span class="pre">${name}</span></code> is the project name normalized according to <a class="reference external" href="https://peps.python.org/pep-0625/">PEP 625</a>
and <code class="docutils literal notranslate"><span class="pre">${version}</span></code> is the version following <a class="reference external" href="https://peps.python.org/pep-0440/">PEP 440</a>.  The project
name normalization transforms all uppercase letters to lowercase,
and replaces all contiguous runs of <code class="docutils literal notranslate"><span class="pre">._-</span></code> characters with a single
underscore.</p>
<p>For example, package <code class="docutils literal notranslate"><span class="pre">Flask-BabelEx</span></code> version <code class="docutils literal notranslate"><span class="pre">1.2.3</span></code> would use
the following filename:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>flask_babelex-1.2.3.tar.gz
</pre></div>
</div>
<p>However, note that source distributions predating PEP 625 are still
commonly found, and they use non-normalized project names, e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>Flask-BabelEx-1.2.3.tar.gz
</pre></div>
</div>
<p>In both instances, the top directory inside the archive has the same
name as the archive filename, minus <code class="docutils literal notranslate"><span class="pre">.tar.gz</span></code> suffix.  Historically,
<code class="docutils literal notranslate"><span class="pre">.zip</span></code> distributions were used as well.</p>
</section>
<section id="binary-distribution-filenames">
<h3>Binary distribution filenames<a class="headerlink" href="#binary-distribution-filenames" title="Link to this heading">¶</a></h3>
<p>The filename of a binary distribution (wheel) has the general form of:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="si">${</span><span class="nv">name</span><span class="si">}</span>-<span class="si">${</span><span class="nv">version</span><span class="si">}</span>-<span class="si">${</span><span class="nv">pytag</span><span class="si">}</span>-<span class="si">${</span><span class="nv">abitag</span><span class="si">}</span>-<span class="si">${</span><span class="nv">platformtag</span><span class="si">}</span>.whl
</pre></div>
</div>
<p>Similarly to source distributions, <code class="docutils literal notranslate"><span class="pre">${name}</span></code> and <code class="docutils literal notranslate"><span class="pre">${version}</span></code>
specify the normalized project name and version (normalization follows
the same rules and is specified in <a class="reference external" href="https://packaging.python.org/en/latest/specifications/binary-distribution-format/">binary distribution format</a>
specification).</p>
<p><code class="docutils literal notranslate"><span class="pre">${pytag}</span></code> is the tag specifying Python version that the wheel was
built for.  This can be <code class="docutils literal notranslate"><span class="pre">py2.py3</span></code> or <code class="docutils literal notranslate"><span class="pre">py3</span></code> for pure Python wheels,
or e.g. <code class="docutils literal notranslate"><span class="pre">cp39</span></code> for CPython 3.9 wheels.  <code class="docutils literal notranslate"><span class="pre">${abitag}</span></code> specifies
the appropriate ABI (<code class="docutils literal notranslate"><span class="pre">none</span></code> for pure Python wheels),
<code class="docutils literal notranslate"><span class="pre">${platformtag}</span></code> the platform (<code class="docutils literal notranslate"><span class="pre">any</span></code> for pure Python wheels).</p>
<p>For example, the modern wheel for the aforementioned package would be
named:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>flask_babelex-1.2.3-py3-none-any.whl
</pre></div>
</div>
<p>However, some distributions use older normalization rules specified
in <a class="reference external" href="https://peps.python.org/pep-0427/">PEP 427</a>.  In this case, runs of characters other than alphanumeric
characters and dots (<code class="docutils literal notranslate"><span class="pre">[^\w\d.]+</span></code>) are replaced by a single underscore.
Notably, this means that uppercase letters and dots are left in place.
In this normalization, the example wheel is named:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>Flask_BabelEx-1.2.3-py3-none-any.whl
</pre></div>
</div>
</section>
</section>
<section id="packages-with-matching-name-and-version">
<h2>Packages with matching name and version<a class="headerlink" href="#packages-with-matching-name-and-version" title="Link to this heading">¶</a></h2>
<p>In the most common case, the upstream package will have exactly the same
name as the Gentoo package, and the version numbers will be entirely
compatible.  In this case, it is sufficient to inherit the eclass,
and it will automatically set a suitable default <code class="docutils literal notranslate"><span class="pre">SRC_URI</span></code>.
The result will be roughly equivalent to:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nv">SRC_URI</span><span class="o">=</span><span class="s2">&quot;</span>
<span class="s2">    https://files.pythonhosted.org/packages/source/</span><span class="si">${</span><span class="nv">PN</span><span class="p">::</span><span class="nv">1</span><span class="si">}</span><span class="s2">/</span><span class="si">${</span><span class="nv">PN</span><span class="si">}</span><span class="s2">/</span><span class="si">${</span><span class="nv">P</span><span class="si">}</span><span class="s2">.tar.gz</span>
<span class="s2">&quot;</span>
<span class="nv">S</span><span class="o">=</span><span class="si">${</span><span class="nv">WORKDIR</span><span class="si">}</span>/<span class="si">${</span><span class="nv">P</span><span class="si">}</span>
</pre></div>
</div>
<p>with <code class="docutils literal notranslate"><span class="pre">${P}</span></code> actually using the normalized project name and the Gentoo
version being translated to its PyPI equivalent.  The version
translation performs the following replacements:</p>
<blockquote>
<div><table class="docutils align-default">
<thead>
<tr class="row-odd"><th class="head"><p>Gentoo suffix</p></th>
<th class="head"><p>PyPI suffix</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>_alpha</p></td>
<td><p>a</p></td>
</tr>
<tr class="row-odd"><td><p>_beta</p></td>
<td><p>b</p></td>
</tr>
<tr class="row-even"><td><p>_rc</p></td>
<td><p>rc</p></td>
</tr>
<tr class="row-odd"><td><p>_p</p></td>
<td><p>.post</p></td>
</tr>
</tbody>
</table>
</div></blockquote>
<p>If the project in question uses a build system that is not compliant
with <a class="reference external" href="https://peps.python.org/pep-0625/">PEP 625</a> and has uppercase letters or dots in its name, you may
need to set <code class="docutils literal notranslate"><span class="pre">PYPI_NO_NORMALIZE</span></code> to a non-empty value to disable name
normalization, e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nv">PYPI_NO_NORMALIZE</span><span class="o">=</span><span class="m">1</span>

inherit<span class="w"> </span>distutils-r1<span class="w"> </span>pypi
</pre></div>
</div>
<p>Note that <code class="docutils literal notranslate"><span class="pre">SRC_URI</span></code> is not combined between eclasses and ebuilds.
Should you need to fetch additional files, you need to explicitly append
to the variable or the default will be overwritten, e.g.:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>inherit<span class="w"> </span>distutils-r1<span class="w"> </span>pypi

<span class="nv">SRC_URI</span><span class="o">+=</span><span class="s2">&quot;</span>
<span class="s2">    https://github.com/pytest-dev/execnet/commit/c0459b92bc4a42b08281e69b8802d24c5d3415d4.patch</span>
<span class="s2">        -&gt; </span><span class="si">${</span><span class="nv">P</span><span class="si">}</span><span class="s2">-pytest-7.2.patch</span>
<span class="s2">&quot;</span>
</pre></div>
</div>
</section>
<section id="package-with-a-different-name">
<h2>Package with a different name<a class="headerlink" href="#package-with-a-different-name" title="Link to this heading">¶</a></h2>
<p>If the project name used on PyPI differs from the Gentoo package name,
the <code class="docutils literal notranslate"><span class="pre">PYPI_PN</span></code> variable can be used to use another name.  This is
especially useful for project that use uppercase letters or dots
in their names.</p>
<p>For example, a package using lowercase package name in Gentoo and title
case on PyPI could use:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nv">PYPI_PN</span><span class="o">=</span><span class="si">${</span><span class="nv">PN</span><span class="p">^</span><span class="si">}</span>

inherit<span class="w"> </span>distutils-r1<span class="w"> </span>pypi
</pre></div>
</div>
<p>Note that projects whose distfiles do not conform to PEP 625 will also
need to explicitly disable filename normalization.  For example,
a package using dot in its filename and setuptools would use:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span><span class="nv">PYPI_NO_NORMALIZE</span><span class="o">=</span><span class="m">1</span>
<span class="nv">PYPI_PN</span><span class="o">=</span><span class="si">${</span><span class="nv">PN</span><span class="p">/-/.</span><span class="si">}</span>

inherit<span class="w"> </span>distutils-r1<span class="w"> </span>pypi
</pre></div>
</div>
</section>
<section id="customizing-the-generated-url">
<h2>Customizing the generated URL<a class="headerlink" href="#customizing-the-generated-url" title="Link to this heading">¶</a></h2>
<p>The default value may not be suitable for your package if it uses
a different project name than the Gentoo package name, a version number
that needs to be translated differently or the legacy <code class="docutils literal notranslate"><span class="pre">.zip</span></code> sdist
format.  The <code class="docutils literal notranslate"><span class="pre">pypi_sdist_url</span></code> function can be used to generate URLs
in that case.  Its usage is:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>pypi_sdist_url<span class="w"> </span><span class="o">[</span>--no-normalize<span class="o">]</span><span class="w"> </span><span class="o">[</span>&lt;project&gt;<span class="w"> </span><span class="o">[</span>&lt;version&gt;<span class="w"> </span><span class="o">[</span>&lt;suffix&gt;<span class="o">]]]</span>
</pre></div>
</div>
<p>with package defaulting to <code class="docutils literal notranslate"><span class="pre">${PYPI_PN}</span></code>, version to translated <code class="docutils literal notranslate"><span class="pre">${PV}</span></code>
and suffix to <code class="docutils literal notranslate"><span class="pre">.tar.gz</span></code>.  The generated filename uses <a class="reference external" href="https://peps.python.org/pep-0625/">PEP 625</a>
normalization, unless <code class="docutils literal notranslate"><span class="pre">--no-normalize</span></code> is provided
(<code class="docutils literal notranslate"><span class="pre">PYPI_NO_NORMALIZE</span></code> does not affect explicit function calls).
For example, the Gentoo <code class="docutils literal notranslate"><span class="pre">dev-python/markups</span></code> package uses title-case
<code class="docutils literal notranslate"><span class="pre">Markups</span></code> project name and legacy filename, and so the ebuild needs
to use:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>inherit<span class="w"> </span>distutils-r1<span class="w"> </span>pypi

<span class="nv">SRC_URI</span><span class="o">=</span><span class="s2">&quot;</span><span class="k">$(</span>pypi_sdist_url<span class="w"> </span>--no-normalize<span class="w"> </span><span class="s2">&quot;</span><span class="si">${</span><span class="nv">PN</span><span class="p">^</span><span class="si">}</span><span class="s2">&quot;</span><span class="k">)</span><span class="s2">&quot;</span>
<span class="nv">S</span><span class="o">=</span><span class="si">${</span><span class="nv">WORKDIR</span><span class="si">}</span>/<span class="si">${</span><span class="nv">P</span><span class="p">^</span><span class="si">}</span>
</pre></div>
</div>
<p>Should the package start using source distributions with normalized
filenames, then only project name would need to be overriden
and the default <code class="docutils literal notranslate"><span class="pre">S</span></code> would be correct (<code class="docutils literal notranslate"><span class="pre">Markups</span></code> and <code class="docutils literal notranslate"><span class="pre">markups</span></code>
normalize the same):</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>inherit<span class="w"> </span>distutils-r1<span class="w"> </span>pypi

<span class="nv">SRC_URI</span><span class="o">=</span><span class="s2">&quot;</span><span class="k">$(</span>pypi_sdist_url<span class="w"> </span><span class="s2">&quot;</span><span class="si">${</span><span class="nv">PN</span><span class="p">^</span><span class="si">}</span><span class="s2">&quot;</span><span class="k">)</span><span class="s2">&quot;</span>
</pre></div>
</div>
<p>Note that due to project name normalization, the ebuild would also work
without <code class="docutils literal notranslate"><span class="pre">SRC_URI</span></code> override.  However, it is recommended to pass
the canonical project name, as normalization is not guaranteed.</p>
</section>
<section id="fetching-wheels">
<h2>Fetching wheels<a class="headerlink" href="#fetching-wheels" title="Link to this heading">¶</a></h2>
<p>In very specific cases, it may be necessary to fetch wheels
(i.e. prebuilt Python packages) instead.  The <code class="docutils literal notranslate"><span class="pre">pypi_wheel_url</span></code>
function is provided to aid this purpose.  Its usage is:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>pypi_wheel_url<span class="w"> </span><span class="o">[</span>--unpack<span class="o">]</span><span class="w"> </span><span class="o">[</span>&lt;project&gt;<span class="w"> </span><span class="o">[</span>&lt;version&gt;<span class="w"> </span><span class="o">[</span>&lt;python-tag&gt;<span class="w"> </span><span class="o">[</span>&lt;abi-platform-tag&gt;<span class="o">]]]]</span>
</pre></div>
</div>
<p>with package defaulting to <code class="docutils literal notranslate"><span class="pre">${PYPI_PN}</span></code>, version to translated <code class="docutils literal notranslate"><span class="pre">${PV}</span></code>,
python-tag to <code class="docutils literal notranslate"><span class="pre">py3</span></code> and abi-platform-tag to <code class="docutils literal notranslate"><span class="pre">none-any</span></code>
(i.e. indicating a pure Python package).  For example,
<code class="docutils literal notranslate"><span class="pre">dev-python/ensurepip-setuptools</span></code> does:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>inherit<span class="w"> </span>pypi
<span class="nv">SRC_URI</span><span class="o">=</span><span class="s2">&quot;</span><span class="k">$(</span>pypi_wheel_url<span class="w"> </span><span class="s2">&quot;</span><span class="si">${</span><span class="nv">PN</span><span class="p">#ensurepip-</span><span class="si">}</span><span class="s2">&quot;</span><span class="k">)</span><span class="s2">&quot;</span>
</pre></div>
</div>
<p>Note that wheels are ZIP archives suffixed <code class="docutils literal notranslate"><span class="pre">.whl</span></code>, and they are not
unpacked by the package manager automatically.  Should you need them
unpacked, you can pass <code class="docutils literal notranslate"><span class="pre">--unpack</span></code> option to include a <code class="docutils literal notranslate"><span class="pre">-&gt;</span></code> operator
that renames the wheel to use <code class="docutils literal notranslate"><span class="pre">.whl.zip</span></code> suffix, causing it to be
unpacked.  Remember to add an explicit dependency on <code class="docutils literal notranslate"><span class="pre">app-arch/unzip</span></code>
in that case.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">pypi_wheel_filename</span></code> function is provided to aid getting
the wheel filename.  It has a matching synopsis:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>pypi_wheel_filename<span class="w"> </span><span class="o">[</span>&lt;project&gt;<span class="w"> </span><span class="o">[</span>&lt;version&gt;<span class="w"> </span><span class="o">[</span>&lt;python-tag&gt;<span class="w"> </span><span class="o">[</span>&lt;abi-platform-tag&gt;<span class="o">]]]]</span>
</pre></div>
</div>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">Gentoo Python Guide</a></h1>









<search id="searchbox" style="display: none" role="search">
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Search"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</search>
<script>document.getElementById('searchbox').style.display = "block"</script><h3>Navigation</h3>
<p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="preface.html">Preface</a></li>
<li class="toctree-l1"><a class="reference internal" href="interpreter.html">Python interpreters</a></li>
<li class="toctree-l1"><a class="reference internal" href="eclass.html">Choosing between Python eclasses</a></li>
<li class="toctree-l1"><a class="reference internal" href="basic.html">Common basics</a></li>
<li class="toctree-l1"><a class="reference internal" href="any.html">python-any-r1 — build-time dependency</a></li>
<li class="toctree-l1"><a class="reference internal" href="single.html">python-single-r1 — single-impl packages</a></li>
<li class="toctree-l1"><a class="reference internal" href="multi.html">python-r1 — multi-impl packages</a></li>
<li class="toctree-l1"><a class="reference internal" href="distutils.html">distutils-r1 — standard Python build systems</a></li>
<li class="toctree-l1"><a class="reference internal" href="test.html">Tests in Python packages</a></li>
<li class="toctree-l1"><a class="reference internal" href="distutils-legacy.html">distutils-r1 legacy concepts</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">pypi — helper eclass for PyPI archives</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#pypi-urls">PyPI URLs</a></li>
<li class="toctree-l2"><a class="reference internal" href="#packages-with-matching-name-and-version">Packages with matching name and version</a></li>
<li class="toctree-l2"><a class="reference internal" href="#package-with-a-different-name">Package with a different name</a></li>
<li class="toctree-l2"><a class="reference internal" href="#customizing-the-generated-url">Customizing the generated URL</a></li>
<li class="toctree-l2"><a class="reference internal" href="#fetching-wheels">Fetching wheels</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="helper.html">Common helper functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="depend.html">Advanced dependencies</a></li>
<li class="toctree-l1"><a class="reference internal" href="pytest.html">pytest recipes</a></li>
<li class="toctree-l1"><a class="reference internal" href="concept.html">Advanced concepts</a></li>
<li class="toctree-l1"><a class="reference internal" href="expert-multi.html">Expert python-r1 usage</a></li>
<li class="toctree-l1"><a class="reference internal" href="buildsys.html">Integration with build systems written in Python</a></li>
<li class="toctree-l1"><a class="reference internal" href="porting.html">Porting tips</a></li>
<li class="toctree-l1"><a class="reference internal" href="migration.html">Migration guides</a></li>
<li class="toctree-l1"><a class="reference internal" href="qawarn.html">QA checks and warnings</a></li>
<li class="toctree-l1"><a class="reference internal" href="package-maintenance.html">Python package maintenance</a></li>
<li class="toctree-l1"><a class="reference internal" href="interpreter-maintenance.html">Maintenance of Python implementations</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="distutils-legacy.html" title="previous chapter">distutils-r1 legacy concepts</a></li>
      <li>Next: <a href="helper.html" title="next chapter">Common helper functions</a></li>
  </ul></li>
</ul>
</div>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &#169;2020, Michał Górny, license: CC BY 4.0.
      
      |
      Powered by <a href="https://www.sphinx-doc.org/">Sphinx 8.1.3</a>
      &amp; <a href="https://alabaster.readthedocs.io">Alabaster 1.0.0</a>
      
      |
      <a href="_sources/pypi.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>